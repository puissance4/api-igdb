var express = require('express');
var bodyParser = require('body-parser');
var apiRouter = require('./apiRouter').router;
var cors = require('cors');
//Instantiation du serveur
var server = express()

//Configuration de body parser
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
server.use(cors());

//Configuration des routes 
server.get('/', function(request, response){
    response.setHeader('Content-Type', 'text/html');
    response.status(200).send('<h1 style="color:lightgreen; background: darkgrey; text-align: center; font-size: 44px">Serveur en marche</h1>');
});

server.use('/api/', apiRouter);

//Lancement du serveur
server.listen(82, function(){
    console.log('Serveur ON - Tout est ok')
});




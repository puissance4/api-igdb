//Imports
var express = require('express');
var newsCtrl = require('./routes/newsCtrl');

// Routeur 
exports.router = (function() {
    var apiRouter = express.Router();

    //News => routes
    apiRouter.route('/vg/getLastNews/').get(newsCtrl.getLastNews);

    //News => routes
    apiRouter.route('/vg/getNewsContent/').get(newsCtrl.getNewsContent);

    //News => routes
    apiRouter.route('/vg/getGame/').get(newsCtrl.getGame);

    return apiRouter;

//Attention les parenthèses sont importantes => instantiation du router
})();

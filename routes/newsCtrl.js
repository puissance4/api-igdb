// Imports
const axios = require('axios');

//Routes
module.exports = {
    //GET
    getLastNews: function(req, res) {
        axios({
          url: "https://api-v3.igdb.com/pulse_groups",
          method: 'GET',
          headers: {
              'Accept': 'application/json',
              'user-key': '2a8cb32ae64399adca49052c2f405617'
          },
          data: "fields * ; limit 10; sort id desc;"
        })
          .then(response => {
            //Iterate through the JSON array
            let r = response.data;
            return res.status(201).json(r);
          })
          .catch(err => {
              console.error(err);
          });
    },

    getNewsContent: function(req, res) {
        axios({
            url: "https://api-v3.igdb.com/pulses",
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'user-key': '2a8cb32ae64399adca49052c2f405617'
            },
            data: "fields created_at,image,pulse_image,pulse_source,summary,title,videos,website; where id = 843502;"
        })
            .then(response => {
                //Iterate through the JSON array
                let r = response.data;
                console.log(response.data);
                for (i=0; i<r.length; ++i){
                    //Store json values
                    console.log(r[i].pulses);
                }
                return res.status(201).json(r);
            })
            .catch(err => {
                console.error(err);
            });
    },

    getGame: function(req, res) {
    axios({
        url: "https://api-v3.igdb.com/games",
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'user-key': '2a8cb32ae64399adca49052c2f405617'
        },
        data: "fields name, summary, url; sort created_at desc;"
    })
        .then(response => {
            //Iterate through the JSON array
            let r = response.data;

            return res.status(201).json(r);
        })
        .catch(err => {
            console.error(err);
        });
    }
}



